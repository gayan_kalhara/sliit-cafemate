package cafemate.sliit.com.cafemate.util;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Gayan Kalhara on 10/2/2016.
 */

public class BarUtils {
    public static final int NO_TITLE_CHANGE = 0;
    public static final int NO_COLOR_CHANGE = 0;

    public static void setupActionBar(AppCompatActivity activity, int titleResource, int actionBarColorResource, boolean setDisplayHomeAsUpEnabled) {
        setupActionBar(activity.getSupportActionBar(), titleResource, actionBarColorResource, setDisplayHomeAsUpEnabled);
    }

    public static void setupActionBar(ActionBar actionBar, int titleResource, int actionBarColorResource, boolean setDisplayHomeAsUpEnabled) {
        if (titleResource != NO_TITLE_CHANGE) {
            setActionBarTitle(actionBar, titleResource);
        }

        if (actionBarColorResource != NO_COLOR_CHANGE) {
            setActionBarColor(actionBar, actionBarColorResource);
        }

        if (setDisplayHomeAsUpEnabled) {
            enableDisplayHomeAsUp(actionBar);
        }
    }

    public static void hideShadow(AppCompatActivity activity){
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar != null) {
            actionBar.setElevation(0);
        }
    }

    public static void setActionBarTitle(ActionBar actionBar, int titleResource) {
        actionBar.setTitle(titleResource);
    }

    public static void setActionBarColor(ActionBar actionBar, int colorResource) {
        actionBar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(actionBar.getThemedContext(), colorResource)));
    }

    public static void enableDisplayHomeAsUp(ActionBar actionBar) {
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public static void hideActionBar(AppCompatActivity activity) {
        hideActionBar(activity.getSupportActionBar());
    }

    public static void hideActionBar(ActionBar actionBar) {
        actionBar.hide();
    }

    public static void setNotificationBarColor(AppCompatActivity activity, int colorResource) {
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(activity, colorResource));
        }

    }

    public static void removeLayoutLimits(AppCompatActivity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = activity.getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    public static void hideNavigationBar(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }
}
