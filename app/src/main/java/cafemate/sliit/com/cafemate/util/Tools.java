package cafemate.sliit.com.cafemate.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import cafemate.sliit.com.cafemate.R;

/**
 * Created by Gayan Kalhara on 10/2/2016.
 */

public class Tools {
    private static float getApiVersion() {

        Float f = null;
        try {
            f = Float.valueOf(android.os.Build.VERSION.RELEASE.substring(0, 2));
        } catch (NumberFormatException e) {
            Log.e("", "API Error" + e.getMessage());
        }

        assert f != null;
        return f;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void systemBarLollipop(Activity act, Context context) {
        if (getApiVersion() >= 5.0) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        }
    }

    /**
     *  Making notification bar transparent
     * */
    public static void changeStatusBarColor(Activity act) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = act.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }
}
