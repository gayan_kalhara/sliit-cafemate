package cafemate.sliit.com.cafemate;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;

import cafemate.sliit.com.cafemate.R;


public class NotificationUtils {
    public static void showSnackbar(View view, int messageResource) {
        Snackbar.make(view, messageResource, Snackbar.LENGTH_LONG).show();
    }

    public static void showColoredSnackbar(View view, String message, int color){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(view.getContext(), color));
        snackbar.show();
    }

}
