package cafemate.sliit.com.cafemate;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import cafemate.sliit.com.cafemate.util.StringUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViews();

        btnLogin.setOnClickListener(this);
    }

    private void findViews() {
        etUsername = (EditText) findViewById(R.id.et_username);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
    }

    private void notifyEmptyUsername() {
        etUsername.requestFocus();
        etUsername.setError(getString(R.string.msg_empty_username));
    }

    private void notifyEmptyPassword() {
        etPassword.requestFocus();
        etPassword.setError(getString(R.string.msg_empty_password));
    }

    private void notifyWrongUsernamePassword() {
        //NotificationUtils.showColoredSnackbar(etUsername, getResources().getString(R.string.msg_wrong_username_password), R.color.incyposLight);
    }

    @Override
    public void onClick(View view) {
        onLoginClick();
    }

    private void onLoginClick() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if (StringUtils.isNullOrEmpty(username)) {
            notifyEmptyUsername();
            return;
        }

        if (StringUtils.isNullOrEmpty(password)) {
            notifyEmptyPassword();
            return;
        }

        if (username.equalsIgnoreCase("cashier")) {
            startActivity(new Intent(this, OrderActivity.class));
            return;
        }


        finish();
    }
}
