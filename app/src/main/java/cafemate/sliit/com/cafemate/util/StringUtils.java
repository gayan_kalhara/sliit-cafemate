package cafemate.sliit.com.cafemate.util;
import android.text.TextUtils;
import android.widget.EditText;

import java.text.DecimalFormat;

public class StringUtils {
    private static final DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty() || string.trim().isEmpty();
    }

    public static String toTwoDecimalString(double number) {
        return decimalFormat.format(number);
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showTextValidateError(EditText editText, String error){
        editText.requestFocus();
        editText.setError(error);
    }
}