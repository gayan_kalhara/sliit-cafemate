package cafemate.sliit.com.cafemate;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import cafemate.sliit.com.cafemate.util.BarUtils;
import cafemate.sliit.com.cafemate.util.RegistrationRetainFragment;
import cafemate.sliit.com.cafemate.util.StringUtils;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, RegistrationRetainFragment.RegistrationCallbacks {
    private static final String TAG_REGISTRATION_RETAIN_FRAGMENT = "TAG_REGISTRATION_RETAIN_FRAGMENT";

    private RegistrationRetainFragment registrationRetainFragment;

    private String customerName;
    private String companyName;
    private String email;
    private String password;

    private EditText etCustomerName;
    private EditText etCompanyName;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etPasswordConfirm;
    private Button btnSignup;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        setupBars();
        findViews();
        setListeners();
        hideKeyboard();
        initRatainFragment();
    }

    private void setupBars() {
        BarUtils.setNotificationBarColor(this, R.color.dark_gray);
    }

    private void findViews() {
        etCustomerName = (EditText) findViewById(R.id.et_customer_name);
        etCompanyName = (EditText) findViewById(R.id.et_company_name);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        etPasswordConfirm = (EditText) findViewById(R.id.et_password_confirm);
        btnSignup = (Button) findViewById(R.id.btn_signup);
    }


    private void setListeners() {
        btnSignup.setOnClickListener(this);
    }

    private void hideKeyboard(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void initRatainFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();

        registrationRetainFragment = (RegistrationRetainFragment) fragmentManager.findFragmentByTag(TAG_REGISTRATION_RETAIN_FRAGMENT);

        if (registrationRetainFragment == null) {
            registrationRetainFragment = new RegistrationRetainFragment();
            fragmentManager.beginTransaction().add(registrationRetainFragment, TAG_REGISTRATION_RETAIN_FRAGMENT).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
    }

    @Override
    protected void onPause() {
        progress.dismiss();
        progress = null;
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                onSignupClick(v);
        }
    }

    private void onSignupClick(View v) {
        customerName = etCustomerName.getText().toString();
        companyName = etCompanyName.getText().toString();
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        String confirmPassword = etPasswordConfirm.getText().toString();

        if (StringUtils.isNullOrEmpty(customerName)) {
            StringUtils.showTextValidateError(etCustomerName, getString(R.string.msg_empty_customer));
            return;
        }

        if (StringUtils.isNullOrEmpty(companyName)) {
            StringUtils.showTextValidateError(etCompanyName, getString(R.string.msg_empty_company));
            return;
        }

        if (StringUtils.isNullOrEmpty(email)) {
            StringUtils.showTextValidateError(etEmail, getString(R.string.msg_empty_email));
            return;
        } else if (!StringUtils.isValidEmail(email)){
            StringUtils.showTextValidateError(etEmail, getString(R.string.msg_invalid_email));
            return;
        }

        if (StringUtils.isNullOrEmpty(password)) {
            StringUtils.showTextValidateError(etPassword, getString(R.string.msg_empty_password));
            return;
        }

        if(!(StringUtils.isNullOrEmpty(password) || StringUtils.isNullOrEmpty(confirmPassword))){
            if(!password.equals(confirmPassword)){
                StringUtils.showTextValidateError(etPasswordConfirm, getString(R.string.msg_not_matching_password));
                return;
            } else{
                if(password.length() < 6){
                    StringUtils.showTextValidateError(etPassword, getString(R.string.msg_password_min_length));
                    return;
                }
            }
        } else{
            if (StringUtils.isNullOrEmpty(password)) {
                StringUtils.showTextValidateError(etPassword, getString(R.string.msg_empty_password_confirm));
                return;
            }

            if (StringUtils.isNullOrEmpty(confirmPassword)) {
                StringUtils.showTextValidateError(etPasswordConfirm, getString(R.string.msg_empty_password_confirm));
                return;
            }
        }

        register();
    }

    public void register() {
        registrationRetainFragment.setData(customerName, email, password, companyName);
        registrationRetainFragment.startRegistration();
    }

    private void setProgressMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progress == null) {
                    return;
                }

                progress.setMessage(message);

                if (!progress.isShowing()) {
                    progress.show();
                }
            }
        });
    }

    private void hideProgressbar() {
        if (progress == null) {
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.hide();
            }
        });
    }

    private void showErrorNotification(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
                builder.setMessage(message)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void showSuccessMessage() {
        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
        finish();
    }


    @Override
    public void onProgress(String message) {
        setProgressMessage(message);
    }

    @Override
    public void onSuccess(String message) {
        hideProgressbar();
        showSuccessMessage();
    }

    @Override
    public void onError(String message) {
        hideProgressbar();
        showErrorNotification(message);
    }
}
